def add(first,second)
  first + second
end

def subtract(first,second)
  first - second
end

def sum(array)
  array.inject(0, :+)
end

def multiply(array)
  array.reduce(:*)
end

def power(base,exponent)
  base ** exponent
end

def factorial(num)
  return 1 if num == 0
  result = 1
  for i in 1..num do
    result *= i
  end
  result
end
