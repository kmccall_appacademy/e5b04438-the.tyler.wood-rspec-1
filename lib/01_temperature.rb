def ftoc(ftemp)
  (ftemp - 32.0) * 5/9.0
end

def ctof(ctemp)
  (ctemp * 9/5.0) + 32.0
end
