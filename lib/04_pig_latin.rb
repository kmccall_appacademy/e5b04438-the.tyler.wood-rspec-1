def translate(phrase)
  words = phrase.split(" ")
  words.each_with_index do |word,index|
    words[index] = pig_latin_translate(word)
  end
  words.join(" ")

end

def pig_latin_translate(word)
  vowels = ["a","e","i","o","u"]
  letters = word.split("")
  searching = true

  if vowels.include?(letters[0])
    return letters.push("ay").join("")
  else
    while searching
      if !vowels.include?(letters[0]) ||
          (letters[-1].downcase == "q" && letters[0].downcase == "u")
        buffer = letters.shift
        letters.push(buffer)
      else
        searching = false
      end
    end
    letters.push("ay")
  end

  letters.join("")
end
