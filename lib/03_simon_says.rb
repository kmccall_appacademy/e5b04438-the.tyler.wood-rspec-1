def echo(input)
  input
end

def shout(input)
  input.upcase
end

def repeat(input, times = 2)
  result = input
  (times-1).times do
    result += " #{input}"
  end
  result
end

def start_of_word(word,letters)
  word.slice(0,letters)
end

def first_word(sentence)
  sentence.split(" ")[0]
end

def titleize(sentence)
  small_words = ["and","over","the"]
  words = sentence.split(" ")
  words[0][0] = words[0][0].upcase
  words.each do |word|
      word[0] = word[0].upcase unless small_words.include?(word)
  end
  words.join(" ")
end
